package com.example.outofdate.opgtest;

import android.opengl.GLSurfaceView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

public class MainActivity extends AppCompatActivity {

    GLSurfaceView surface;


    static {
        System.loadLibrary("interface");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        surface = (GLSurfaceView) findViewById(R.id.gl);
        surface.setEGLContextFactory(new GLSurfaceView.EGLContextFactory() {

            private int EGL_CONTEXT_CLIENT_VERSION = 0x3098;

            @Override
            public EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig eglConfig) {
                int[] attrib_list = {EGL_CONTEXT_CLIENT_VERSION, 2, EGL10.EGL_NONE};
                return egl.eglCreateContext(display, eglConfig, EGL10.EGL_NO_CONTEXT, attrib_list);
            }

            @Override
            public void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context) {

            }
        });
        surface.setRenderer(new GLSurfaceView.Renderer() {
            @Override
            public void onSurfaceCreated(GL10 gl, EGLConfig config) {

            }

            @Override
            public void onSurfaceChanged(GL10 gl, int width, int height) {
                JniWrapper.setup(width, height);
            }

            @Override
            public void onDrawFrame(GL10 gl) {
                JniWrapper.draw();
            }
        });
    }
}
