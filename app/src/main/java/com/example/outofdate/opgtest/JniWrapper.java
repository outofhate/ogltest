package com.example.outofdate.opgtest;

/**
 * Created by outofdate on 04.06.2016.
 *
 * @author outofdate
 */
public class JniWrapper {

    private static final String TAG = "JniWrapper";

    public static native void draw();

    public static native void setup(int width, int height);
}
