//
// Created by outofdate on 04.06.2016.
//
#include <jni.h>

#include <GLES2/gl2.h>

#define NANOVG_GLES2_IMPLEMENTATION
#include "nanovg/nanovg.h"
#include "nanovg/nanovg_gl.h"

struct NVGcontext *vg;

int w, h;

void draw();

extern "C" {
JNIEXPORT void JNICALL Java_com_example_outofdate_opgtest_JniWrapper_draw(JNIEnv *env,
                                                                          jobject obj);

JNIEXPORT void JNICALL Java_com_example_outofdate_opgtest_JniWrapper_setup(JNIEnv *env,
                                                                           jobject obj, jint _w,
                                                                           jint _h);
}

void Java_com_example_outofdate_opgtest_JniWrapper_setup(JNIEnv *env, jobject obj, jint _w,
                                                         jint _h) {
    w = _w;
    h = _h;
    vg = nvgCreateGLES2(NVG_ANTIALIAS | NVG_STENCIL_STROKES | NVG_DEBUG);
    glViewport(0, 0, w, h);
    glClearColor(0, 0, 0, 255);
}

JNIEXPORT void JNICALL Java_com_example_outofdate_opgtest_JniWrapper_draw(JNIEnv *env,
                                                                          jobject obj) {
    draw();
}

void draw() {
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    nvgBeginFrame(vg, w, h, 1);
    nvgBeginPath(vg);
    nvgMoveTo(vg, 400, 200);
    nvgLineTo(vg, 200, 450);
    nvgLineTo(vg, 600, 450);
    nvgClosePath(vg);
    nvgFillColor(vg, nvgRGBA(255, 192, 0, 255));
    nvgFill(vg);
    nvgEndFrame(vg);
}